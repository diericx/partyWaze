//
//  ViewController.swift
//  PartyWaze
//
//  Created by Zac Holland on 12/25/16.
//  Copyright © 2016 Diericx. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {
    
    func authCallback(user: FIRUser?) {
        if (user == nil) {
            print("Sign in failed!")
        } else {
            print("Sign in succeded!")
            print("USER: ", user?.email);
            //Set the users location in the database
            FirebaseData.setLocation(lat: 0, long: 0);
            //Get users near this location
            FirebaseData.getNearbyUsers(lat: 0, long: 0)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //Initialize Firebase
        FirebaseData.initialize()
        //----AUTHENTICATION----
        FirebaseData.signin(email: "zac@appdojo.com", password: "xxxxxxxx", callback: authCallback);
        //FirebaseData.signup(email: "zac@appdojo.com", password: "xxxxxxxx", phone: 6507148055, callback: authCallback)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


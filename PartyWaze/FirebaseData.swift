//
//  FirebaseData.swift
//  PartyWaze
//
//  Created by Zac Holland on 12/25/16.
//  Copyright © 2016 Diericx. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseAuth

class FirebaseData {
    static var ref: FIRDatabaseReference!
    static var currentUserRef: FIRDatabaseReference!
    static var currentUser: FIRUser?
    
    static func initialize() {
        FIRApp.configure()
        self.ref = FIRDatabase.database().reference()
    }
    
    //AUTH
    static func signup(email: String, password: String, phone: Int, callback: @escaping (FIRUser?) -> Void) {
        FIRAuth.auth()?.createUser(withEmail: email, password: password) { (user, error) in
            if (error == nil) {
                self.currentUser = user
                self.currentUserRef = self.ref.child("users").child(user!.uid)
                self.currentUserRef.setValue(["phone": phone])
            } else {
                print(error)
            }
            callback(user)
        }
    }
    
    static func signin(email: String, password: String, callback: @escaping (FIRUser?) -> Void) {
        FIRAuth.auth()?.signIn(withEmail: email, password: password) { (user, error) in
            if (error == nil) {
                self.currentUser = user
            } else {
                print(error)
            }
            callback(user)
        }
    }
    
    static func signout() {
        FIRAuth.auth()?.signOut()
    }
    
    //LOCATION
    static func setLocation(lat: Double, long: Double) -> Bool {
        if (self.currentUser != nil) {
            self.ref.child("users/"+self.currentUser!.uid+"/lat").setValue(lat)
            self.ref.child("users/"+self.currentUser!.uid+"/long").setValue(long)
            return true
        } else {
            return false;
        }
    }

    static func getNearbyUsers(lat: Double, long: Double) -> Dictionary<String, NSDictionary>? {
        var usersDict = Dictionary<String, NSDictionary>()
        if (self.currentUser != nil) {
            self.ref.child("users").observe(.value, with: { snapshot in
                print(snapshot.childrenCount) // I got the expected number of items
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                    let child = rest.value! as! NSDictionary
                    usersDict[rest.key] = child
                }
            })
            return usersDict
        } else {
            return nil;
        }
        
    }
}
